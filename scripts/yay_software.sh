#!/bin/bash

# YAY package install script

source ./bash_colors

echo -e "${BLUE}Installing yay sofware...${NC}"
yay -S acct aide foobar2000 modprobed-db inxi brave-bin mpvpaper sbupdate-git \
   icons-in-terminal appimagetool-bin appimagelauncher app-outlet-bin nwg-drawer
echo -e "${GREEN}INSTALL COMPLETE${NC}\n"
