#!/bin/bash

# Package configs

source ./bash_colors

priv=sudo

#Desktop and Themes
echo -e "${BLUE}Installing Themes...${NC}"
for name in lxappearance kvantum adapta-gtk-theme arc-gtk-theme pop-gtk-theme \
    arc-icon-theme deepin-icon-theme pop-icon-theme asciiquarium \
    noto-fonts-emoji ttf-nerd-fonts-symbols-2048-em ttf-liberation
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
        elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           $priv pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
              echo -e "${GREEN}Installed:  $name${NC}"
            else
              echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
      else
          echo -e "${RED}$name was not installed.${NC}"
    fi
done
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"

#Add .vim .vimrc .bashrc and vim setup
echo -e "${BLUE}Generating Bash/Vim Configs...${NC}"
for name in opendoas
do
    if pacman -Q | [[ $(grep -o "\bopendoas\b" 2>/dev/null) ]]
        then
            break
        else
            echo -e "${RED}Check Installation of Depenedency opendoas${NC}"
            exit 1
    fi
done
mkdir -p $HOME/Downloads
mkdir -p $HOME/Documents
mkdir -p $HOME/Pictures
mkdir -p $HOME/Music
mkdir -p $HOME/Videos
mkdir -p $HOME/Games
mkdir -p $HOME/Desktop
mkdir -p $HOME/.config
mkdir -p $HOME/Applications
mkdir -p $HOME/Virtual_Machines/vmshare
cp -r ../wallpapers $HOME/Pictures
$priv cp ../.config/my_configs/doas.conf /etc/doas.conf ;
cp ../.config/my_configs/.bashrc $HOME/.bashrc
cp ../.config/my_configs/.vimrc $HOME/.vimrc
cp -r ../.config/* $HOME/.config
curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p $HOME/.vim/plugged $HOME/.vim/undodir
vim -es -u $HOME/.vimrc -i NONE -c "PlugInstall" -c "qa"
$priv usermod -a -G seat $USER
$priv usermod -a -G wheel $USER
$priv usermod -a -G audio $USER
$priv usermod -a -G video $USER
$priv usermod -a -G libvirt $USER
$priv usermod -a -G wireshark $USER
$priv systemctl enable seatd.service
$priv systemctl enable libvirtd.service
echo -e "${GREEN}Bash/Vim Configs Generated${NC}\n"
