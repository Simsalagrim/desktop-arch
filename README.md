# Desktop #   
A collection of repos, commands, scripts and configs to setup arch linux with a privacy and security focus.   

## Description ##   

## Installation       

### Setup Scripts      
* git clone https://gitlab.com/Simsalagrim/desktop-arch.git
* cd setup   
* sudo chmod 750 setup.sh   
* ./setup.sh   
If there is a package missing or an error during the install run the setup.sh again.   

## Roadmap    

## License    
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007   

## Project status    
Currently under development   

## Repos    
