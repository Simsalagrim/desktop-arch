#!/bin/bash
# Custom kernel builder script for linux-hardened

source ./bash_colors

configs=../kernel.configs
ver=hardened
priv=sudo

function dependencies()
{
echo -e "${BLUE}Dependency Installation...${NC}"
for name in linux-hardened-headers base-devel xmlto kmod bc libelf git cpio \
    perl tar xz inetutils
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
        elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           $priv pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
              echo -e "${GREEN}Installed:  $name${NC}"
            else
              echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
      else
          echo -e "${RED}$name was not installed.${NC}"
    fi
done
echo
echo -e "${GREEN}Dependencies installed${NC}\n"
}
function dload()
{
    wget "https://github.com/anthraxx/linux-$ver/archive/refs/tags/$rel-$ver$verno.tar.gz"
    tar -xzf "$rel-$ver$verno.tar.gz"
    $priv rm -r "$rel-$ver$verno.tar.gz" 2> /dev/null
    $priv rm -r "/boot/vmlinuz-linux-$ver$verno" 2> /dev/null
    $priv rm -r "/boot/initramfs-linux-$ver$verno.img" 2> /dev/null
    $priv rm -r "/boot/initramfs-linux-$ver$verno-fallback.img" 2> /dev/null
    $priv rm -r "/etc/mkinitcpio.d/linux-$ver$verno.preset" 2> /dev/null
    cd "linux-$ver-$rel-$ver$verno"
    make mrproper
}    

#Select Kernel Options
echo -e "${BBLUE}### KERNEL BUILD SCRIPT ###${NC}"
echo -e "${BLUE}The following dependencies will be required:${NC}"
echo linux-hardened-headers base-devel xmlto kmod bc libelf git cpio \
    perl tar xz inetutils
echo
echo -e "${PURPLE}Continue?${NC}"
select depends in Yes No
do
   case $depends in
       Yes)
            dependencies
            break ;;
        No)
            exit ;;
         *) 
            echo -e "${RED}Invalid Input${NC}"
            continue ;;
    esac
done
echo -e "${BLUE}Current Kernel Version${NC}" 
uname -r | awk -F "-" '{print $1}'
echo -e "${BLUE}Enter New Kernel Release${NC}"
read rel
echo -e "rel=$rel" > ./version.txt
echo -e "${BLUE}Current Kernel Local Version${NC}"
uname -r | awk -F "-" '{print $2}'
echo -e "${BLUE}Enter Kernel Version Number (Default 1)${NC}"
read verno 
echo -e "ver=linux-$ver$verno" >> ./version.txt
echo
echo -e "${BLUE}Select Kernel Config to Build${NC}"
select kconfig in Current New
do
    case $kconfig in
        Current)
            dload
            zcat /proc/config.gz > .config 
            make olddefconfig
            make nconfig
            break ;;
            New)
            dload
            echo
            echo -e "${BLUE}Available Configs${NC}"
            ls -a $configs
            echo
            echo -e "${BLUE}Select a config${NC}"
            read selconf
            cp $configs/$selconf ./.config
            make olddefconfig
            make nconfig
            break ;;
        *)
            echo -e "${RED}Invalid Input${NC}" 
            echo -e "${BLUE}Select Kernel Config to Build${NC}"
            continue ;;
    esac
done
echo
echo -e "${PURPLE}Compile Kernel${NC}"
select build in Yes No
do
    case $build in
        Yes)
            break ;;
         No)
            exit ;;
          *)
            echo -e "${RED}Invalid Input${NC}"
            continue ;;
    esac
done

#Build
echo
echo -e "${BLUE}Compiling Kernel${NC}"
make -j16
make modules -j16
echo
echo -e "${BLUE}Installing Modules${NC}"
$priv make modules_install
echo
echo -e "${BLUE}Making inintcpio${NC}"
$priv cp -v "arch/x86/boot/bzImage" "/boot/vmlinuz-linux-$ver$verno"
echo "# mkinitcpio preset file for the 'linux-$ver$verno' package

ALL_config=\"/etc/mkinitcpio.conf\"
ALL_kver=\"/boot/vmlinuz-linux-$ver$verno\"

PRESETS=('default' 'fallback')

#default_config=\"/etc/mkinitcpio.conf\"
default_image=\"/boot/initramfs-linux-$ver$verno.img\"
#default_options=\"\"

#fallback_config=\"/etc/mkinitcpio.conf\"
fallback_image=\"/boot/initramfs-linux-$ver$verno-fallback.img\"
fallback_options=\"-S autodetect\"" | $priv tee -a  "/etc/mkinitcpio.d/linux-$ver$verno.preset"
$priv mkinitcpio -p linux-$ver$verno
cd ..
echo -e "${GREEN}Kernel Built${NC}\n"
