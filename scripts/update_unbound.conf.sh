#!/bin/bash

#Updates Unbound Conf to Set Values

source ./bash_colors

priv=doas
rootdir=/etc/unbound
ogconf=$rootdir/unbound.conf
newconf=$ogconf.pacnew
oldconf=$ogconf.old

echo -e "${BBLUE}Starting Update of Unbound.conf${NC}"
echo -e "${BLUE}Proposed changes to unbound.conf${NC}"
diff $ogconf $newconf
echo
echo -e "${GREEN}Updating Unbound.conf...${NC}"
$priv sed --in-place   's|#include: "otherfile.conf"|include: "/etc/unbound/resolvconf.conf"|g' $newconf
$priv sed --in-place   's/# num-threads: 1/  num-threads: 8/g' $newconf
$priv sed --in-place   's/# port: 53/  port: 53/g' $newconf
$priv sed --in-place   's/# msg-cache-size: 4m/  msg-cache-size: 256m/g' $newconf
$priv sed --in-place   's/# msg-cache-slabs: 4/  msg-cache-slabs: 16/g' $newconf
$priv sed --in-place   's/# rrset-cache-size: 4m/  rrset-cache-size: 500m/g' $newconf
$priv sed --in-place   's/# rrset-cache-slabs: 4/  rrset-cache-slabs: 16/g' $newconf
$priv sed --in-place   's/# cache-min-ttl: 0/  cache-min-ttl: 3600/g' $newconf
$priv sed --in-place   's/# cache-max-ttl: 86400/  cache-max-ttl: 86400/g' $newconf
$priv sed --in-place   's/# infra-cache-slabs: 4/  infra-cache-slabs: 16/g' $newconf
$priv sed --in-place   's/# do-ip4: yes/  do-ip4: yes/g' $newconf
$priv sed --in-place   's/# do-ip6: yes/  do-ip6: yes/g' $newconf
$priv sed --in-place   's/# do-udp: yes/  do-udp: yes/g' $newconf
$priv sed --in-place   's/# do-tcp: yes/  do-tcp: yes/g' $newconf
$priv sed --in-place   's|# root-hints: ""|  root-hints: "/etc/unbound/root.hints"|g' $newconf
$priv sed --in-place   's/# hide-identity: no/  hide-identity: yes/g' $newconf
$priv sed --in-place   's/# hide-version: no/  hide-version: yes/g' $newconf
$priv sed --in-place   's/# harden-glue: yes/  harden-glue: yes/g' $newconf
$priv sed --in-place   's/# harden-dnssec-stripped: yes/  harden-dnssec-stripped: yes/g' $newconf
$priv sed --in-place   's/# use-caps-for-id: no/  use-caps-for-id: yes/g' $newconf
$priv sed --in-place   's|# private-address: 10.0.0.0/8|  private-address: 10.0.0.0/8|g' $newconf
$priv sed --in-place   's|# private-address: 172.16.0.0/12|  private-address: 172.16.0.0/12|g' $newconf
$priv sed --in-place   's|# private-address: 192.168.0.0/16|  private-address: 192.168.0.0/16|g' $newconf
$priv sed --in-place   's|# private-address: 169.254.0.0/16|  private-address: 169.254.0.0/16|g' $newconf
$priv sed --in-place   's|# private-address: fd00::/8|  private-address: fd00::/8|g' $newconf
$priv sed --in-place   's|# private-address: fe80::/10|  private-address: fe80::/10|g' $newconf
$priv sed --in-place   's|# private-address: ::ffff:0:0/96|  private-address: ::ffff:0:0/96|g' $newconf
$priv sed --in-place   's/# private-domain: "example.com"/  private-domain: "home.lan"/g' $newconf
$priv sed --in-place   's/# unwanted-reply-threshold: 0/  unwanted-reply-threshold: 10000/g' $newconf
$priv sed --in-place   's/# do-not-query-localhost: yes/  do-not-query-localhost: yes/g' $newconf
$priv sed --in-place   's/# prefetch: no/  prefetch: yes/g' $newconf
$priv sed --in-place   's/# val-clean-additional: yes/  val-clean-additional: yes/g' $newconf
$priv sed --in-place   's/# key-cache-slabs: 4/  key-cache-slabs: 16/g' $newconf
$priv sed --in-place   's/# tls-system-cert: no/  tls-system-cert: yes/g' $newconf
$priv sed --in-place   's|# tls-cert-bundle: ""|  tls-cert-bundle: /etc/ssl/certs/ca-certificates.crt|g' $newconf
$priv sed --in-place \
    '0,/# forward-zone:/{s/# forward-zone:/forward-zone: \
    name: "." \
    forward-tls-upstream: yes \
    # Quad9 \
    forward-addr: 9.9.9.9@853#dns.quad9.net \
    forward-addr: 2620:fe::fe@853#dns.quad9.net \
    forward-addr: 149.112.112.112@853#dns.quad9.net \
    forward-addr: 2620:fe::9@853#dns.quad9.net \
    # Cloudflare \
    forward-addr: 1.1.1.1@853#cloudflare-dns.com \
    forward-addr: 2606:4700:4700::1111@853#cloudflare-dns.com \
    forward-addr: 1.0.0.1@853#cloudflare-dns.com \
    forward-addr: 2606:4700:4700::1001@853#cloudflare-dns.com \
# forward-zone:/}' $newconf
echo -e "${BLUE}Unbound.conf after changes have taken place${NC}"
diff $ogconf $newconf
$priv mv $ogconf $oldconf
$priv mv $newconf $ogconf
echo
echo -e "${BLUE}Check unbound.conf for errors${NC}"
unbound-checkconf
echo
echo -e "${GREEN}Unbound.conf updated refreshing root.hints${NC}"
$priv curl --output $rootdir/root.hints https://www.internic.net/domain/named.cache
$priv systemctl enable unbound.service
$priv systemctl start unbound.service
$priv systemctl restart unbound.service
echo
echo -e "${BLUE}Unbound.service started checking DNSSEC validation${NC}"
unbound-host -C $ogconf -v sigok.verteiltesysteme.net
unbound-host -C $ogconf -v sigfail.verteiltesysteme.net
echo -e "${GREEN}Unbound.conf Updated VALIDATION COMPLETE${NC}\n"

