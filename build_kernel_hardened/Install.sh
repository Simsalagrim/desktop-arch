#!/bin/bash
source ./bash_colors

#Kernel_Build
source ./Custom_Kernel.sh

#Unified_Image_Build
source ./Unified_Kernel_Image.sh
echo -e "${GREEN}BUILD COMPLETE${NC}"

#Select Reboot y/n
n=0
for (($n ==0; $n <=5; n++))
    do
        echo -e
        echo -e "${BLUE}Do you want to reboot now? y/n${NC}"
        read reboot1
        case $reboot1 in
            Y) reboot ;;
            y) reboot ;;
            N) exit ;;
            n) exit ;;
            *) echo -e "${RED}Ivalid Input${NC}"
        esac
    continue
done
echo
echo -e "${RED}REBOOT REQUIRED${NC}"
sleep 3

#Reboot_Timer
echo -e "${RED}Restart in 60 secs${NC}" ; \
secs=$((60))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done ; \ 
reboot ;
