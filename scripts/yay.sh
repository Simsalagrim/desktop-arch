#!/bin/bash

# YAY install script

source ./bash_colors

priv=sudo

echo -e "${BLUE}Installing yay...${NC}"
$priv rm -r $HOME/Downloads/yay 2>/dev/null
mkdir -p $HOME/Downloads
git clone https://aur.archlinux.org/yay.git $HOME/Downloads/yay
cd $HOME/Downloads/yay
makepkg -si
cd
$priv rm -r $HOME/Downloads/yay
echo -e "${GREEN}INSTALL COMPLETE${NC}\n"
