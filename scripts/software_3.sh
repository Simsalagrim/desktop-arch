#!/bin/bash

# Package install script no.3

source ./bash_colors

priv=sudo

echo -e  "${BLUE}Software Installation...${NC}"
for name in  vlc gimp keepassxc steam steam-native-runtime lutris wine-staging docker \
    arch-wiki-docs neofetch powerline powerline-fonts android-tools rtl-sdr gqrx urh \
    cmake binwalk openocd flashrom pulseview stlink libsigrok minicom ghidra \
    arm-none-eabi-gcc arm-none-eabi-newlib arm-none-eabi-binutils arm-none-eabi-gdb \
    libreoffice-fresh eog evince gnome-disk-utility htop element-desktop virt-manager \
    qemu-full virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat python-pip \
    mono nodejs npm jdk17-openjdk jre17-openjdk 
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
        elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           $priv pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
              echo -e "${GREEN}Installed:  $name${NC}"
            else
              echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
      else
          echo -e "${RED}$name was not installed.${NC}"
    fi
done  
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"
