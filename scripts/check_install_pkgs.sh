#!/bin/bash

# Checks installed packages

source ./bash_colors

echo -e  "${BLUE}Checking Installation...${NC}" ;
for name in base linux-firmware amd-ucode lvm2 linux-lts \
    linux-lts-headers efibootmgr efitools vim opendoas xterm firefox dhcpcd \
    make git curl wget man-db man-pages pam go iptables \
    archlinux-keyring autoconf automake binutils bison debugedit fakeroot \
    file findutils flex gawk gcc gettext grep groff gzip libtool m4 make \
    pacman patch pkgconf sed sudo texinfo which \
    alacritty sway wofi pcmanfm-gtk3 waybar xorg-xwayland sbsigntools \
    swaybg networkmanager autotiling-rs ntfs-3g ntp swaylock swayidle gnome-polkit \
    xorg-server i3-wm picom polybar  rofi \
    xorg-init xf86-video-amdgpu pipewire pipewire-pulse pipewire-alsa lib32-pipe-jack \
    vulkan-radeon vulkan-intel vulkan-tools lib32-vulkan-radeon lib32-vulkan-intel \
    feh \
    arch-audit sysstat wireshark-qt apparmor traceroute firejail \
    lynis nmap vulscan unbound logrotate clamav rkhunter exa \
    openresolv usbguard rng-tools pavucontrol usbutils exfat-utils ethtool \
    lm_sensors cronie hwinfo rsync shellcheck pam-u2f \
    vlc gimp keepassxc steam lutris wine-staging docker \
    arch-wiki-docs neofetch powerline powerline-fonts android-tools \
    rtl-sdr gqrx urh cmake binwalk openocd flashrom pulseview \
    stlink libsigrok minicom ghidra libreoffice-fresh eog evince gnome-disk-utility \
    htop element-desktop virt-manager qemu-full virt-viewer dnsmasq vde2 bridge-utils \
    openbsd-netcat python-pip mono nodjs npm jdk17-openjdk jre17-openjdk \
    arm-none-eabi-gcc arm-none-eabi-newlib arm-none-eabi-binutils arm-none-eabi-gdb \
    lxappearance kvantum adapta-gtk-theme arc-gtk-theme pop-gtk-theme \
    arc-icon-theme deepin-icon-theme pop-icon-theme asciiquarium \
    noto-fonts-emoji ttf-nerd-fonts-symbols-2048-em ttf-liberation
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
      then
          continue
      else pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; } 
          echo -e "${RED}$name was not installed.${NC}" ;
    fi
done  
echo -e  "${GREEN}Installation Checked!${NC}\n" ;
