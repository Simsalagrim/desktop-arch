#!/bin/bash
# Create signed unified kernel image from .efi

source ./bash_colors
source ./version.txt

#rel=6.1.15
#ver=linux-hardened1
EFI_NAME="/boot/$ver.efi"
INITRAMFS="/boot/initramfs-$ver.img"
VMLINUZ="/boot/vmlinuz-$ver"
LOADER="$ver-signed.efi"
UEFI="/efi/$ver-signed.efi"
BOOTIMG="/vmlinuz-$ver"
priv=sudo

echo -e "${BBLUE}UNIFIED KERNEL IMAGE SCRIPT${NC}"
#Update cmdline args
$priv cp /etc/kernel/cmdline ./
$priv chmod 744 cmdline
sed -i "s#BOOT_IMAGE=[^[:blank:]]*#BOOT_IMAGE=$BOOTIMG#" ./cmdline
$priv chmod 444 cmdline
echo -e "${BLUE}Command Line Args${NC}"
cat /etc/kernel/cmdline
echo

#Create image
echo -e "${BLUE}Creating UEFI Image${NC}"
$priv rm -r "$EFI_NAME" 2> /dev/null
$priv rm -r "$UEFI" 2> /dev/null
$priv cat /boot/amd-ucode.img "$INITRAMFS" > /tmp/combined_initrd.img
$priv objcopy \
    --add-section .osrel="/usr/lib/os-release" --change-section-vma .osrel=0x20000 \
    --add-section .cmdline="/etc/kernel/cmdline" --change-section-vma .cmdline=0x30000 \
    --add-section .splash="/usr/share/systemd/bootctl/splash-arch.bmp" --change-section-vma .splash=0x40000 \
    --add-section .linux="$VMLINUZ" --change-section-vma .linux=0x2000000 \
    --add-section .initrd="/tmp/combined_initrd.img" --change-section-vma .initrd=0x3000000 \
    "/usr/lib/systemd/boot/efi/linuxx64.efi.stub" "$EFI_NAME"
$priv sbsign --key "/etc/efi-keys/db.key" --cert "/etc/efi-keys/db.crt" --output "$EFI_NAME" "$EFI_NAME"
$priv mv "$EFI_NAME" "$UEFI"
echo
echo -e "${BLUE}Creating EFI Boot Entry${NC}"
$priv efibootmgr --create --disk /dev/nvme0n1p1 --part 1 --label "$ver-$rel" --loader "$LOADER" --verbose
echo -e "${GREEN}UEFI Image Created\n${NC}"
