#!/bin/bash

# Package install script no.1

source ./bash_colors

priv=sudo

echo -e "${BLUE}Initial Package Installation...${NC}"
for name in base linux-firmware amd-ucode lvm2 linux-lts base-devel \
    linux-lts-headers efibootmgr vim opendoas xterm firefox dhcpcd make git \
    curl wget man-db man-pages pam go iptables alacritty sway wofi waybar rofi \
    pcmanfm-gtk3 xorg-xwayland sbsigntools swaybg networkmanager autotiling-rs \
    swaylock swayidle ntp ntfs-3g gnome-polkit xorg-server picom i3-wm polybar \
    xorg-init xf86-video-amdgpu pipewire pipewire-pulse pipewire-alsa lib32-pipe-jack \ 
    vulkan-radeon vulkan-intel vulkan-tools lib32-vulkan-radeon lib32-vulkan-intel \
    feh
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
        elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
           $priv pacman -S --noconfirm --needed $name
            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
              then
              echo -e "${GREEN}Installed:  $name${NC}"
            else
              echo -e "${RED}Check Installation of Depenedencies${NC}"
            fi
      else
          echo -e "${RED}$name was not installed.${NC}"
    fi
done  
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"
