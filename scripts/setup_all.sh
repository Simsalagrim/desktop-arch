#!/bin/bash

# Source all setup scripts

source ./bash_colors

priv=sudo

echo -e "${BBLUE}Arch Setup${NC}"
echo -e "${BLUE}Are you logged into the user you wish to run the setup for?${NC}"
select logged in Yes No
do
    case $logged in
        Yes)
            break ;;
        No)
            exit ;;
    esac
done

#$priv useradd -mG wheel
#echo '%wheel ALL=(ALL:ALL) ALL' | EDITOR='tee -a' vi$priv
#echo 'Enter a passwd for user '
#$priv passwd 
#login 
echo -e "${BLUE}Preparing Scripts...${NC}"
$priv rm /var/lib/pacman/db.lck 2>/dev/null ;
chmod 750 ./iptables.sh ./software_1.sh \
 ./software_2.sh ./software_3.sh ./check_install_pkgs.sh \
 ./software_config.sh ./hardening.sh ./DNS_resolver_setup.sh \
 ./yay.sh ./yay_software.sh
chmod 740 ./usbguard.sh
$priv sed --in-place 's|#\[multilib\]|\[multilib\] \
Include = /etc/pacman.d/mirrorlist|g' /etc/pacman.conf
echo
echo -e "${BLUE}Updating Package List...${NC}"
$priv pacman -Sy --noconfirm --needed
echo -e "${GREEN}Package List Updated${NC}\n"
source ./iptables.sh
echo -e "${BLUE}Upgrading Packages...${NC}"
$priv pacman -Syu --noconfirm --needed
echo -e "${GREEN}Packages Upgraded${NC}\n"
source ./software_1.sh
source ./software_2.sh
source ./software_3.sh
source ./software_config.sh
source ./check_install_pkgs.sh
source ./hardening.sh
source ./yay.sh
#source ./yay_software.sh
echo -e "${GREEN}SETUP COMPLETE${NC}"
