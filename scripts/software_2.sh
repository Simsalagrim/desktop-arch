#!/bin/bash

# Package install script no.2

source ./bash_colors

priv=sudo

echo -e "${BLUE}Extra Utils Installation...${NC}"
for name in arch-audit sysstat wireshark-qt apparmor traceroute firejail \
    lynis nmap vulscan unbound logrotate clamav rkhunter exa \
    openresolv usbguard rng-tools pavucontrol usbutils exfat-utils \
    ethtool lm_sensors cronie hwinfo rsync aspell-en shellcheck pam-u2f 
do
    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
        then
            echo -e "${GREEN}Already Installed:  $name${NC}"
            continue
    elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
        then
            $priv pacman -S --noconfirm --needed $name
                if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
                    then
                        echo -e "${GREEN}Installed:  $name${NC}"
                else
                        echo -e "${RED}Check Installation of Depenedencies${NC}"
                fi
    else
          echo -e "${RED}$name was not installed.${NC}"
    fi
done  
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"
