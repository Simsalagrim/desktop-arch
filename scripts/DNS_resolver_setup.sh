#!/bin/bash

# DNS resolver setup

source ./bash_colors

echo -e "${BLUE}Configuring Resolver...${NC}"
sudo cp ../.config/my_configs/resolved.conf /etc/systemd/resolved.conf
sudo systemctl disable systemd-resolved.service
sudo systemctl mask systemd-resolved.service
sudo cp ../.config/my_configs/resolvconf.conf /etc/resolvconf.conf
sudo resolvconf -u
sleep 5
echo -e "${GREEN}Resolver Configured${NC}"

