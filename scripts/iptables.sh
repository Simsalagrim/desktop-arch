#!/bin/bash
# IPTables Setup Script 

priv=sudo

$priv iptables -F ; \
$priv iptables -P INPUT DROP ; \
$priv iptables -P FORWARD DROP ; \
$priv iptables -A INPUT -i lo -j ACCEPT ; \
$priv iptables -A OUTPUT -o lo -j ACCEPT ; \
$priv iptables -A INPUT -j ACCEPT -m conntrack --ctstate ESTABLISHED,RELATED ; \
$priv iptables -A OUTPUT -j ACCEPT -m conntrack --ctstate ESTABLISHED,RELATED ; \
$priv iptables -L -v -n --line-numbers
$priv iptables-save -f /etc/iptables/iptables.rules
$priv systemctl enable iptables.service

